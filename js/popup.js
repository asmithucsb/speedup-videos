window.addEventListener('DOMContentLoaded', function () {
  var pause = false;
  chrome.storage.sync.get("pause", function(items) {
    if (!chrome.runtime.error) {
      pause = items.pause
      document.getElementById("stopOnPopup").checked = items.pause;
    }
  });

  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function (tabs) {
    chrome.tabs.sendMessage(
        tabs[0].id,
        {
          from: 'dataload',
          pause: pause,
        },
        function(response){
          resp = JSON.parse(response);
          document.getElementById('inputspeedvideo').value = resp.video;
          document.getElementById('inputspeedaudio').value = resp.audio;
        });
  });

  document.getElementById('stopOnPopup').addEventListener('click', function() {
    chrome.storage.sync.set(
      { "pause" : document.getElementById('stopOnPopup').checked }
      , function() {
        if (chrome.runtime.error) {
          console.log("Runtime error.");
        }
      });
  });

  document.getElementById('abouturl').addEventListener('click', function() {
    chrome.tabs.create(
      { url: "https://bitbucket.org/asmithucsb/speedup-videos" }
    );
  });

  document.getElementById('setSpeedVideo').addEventListener('click', function() {
    speed = document.getElementById('inputspeedvideo').value;
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      chrome.tabs.sendMessage(
          tabs[0].id,
          {from: 'popup', subject: 'Video', speed: speed,},
          function(response){});
    });
  });

  document.getElementById('setSpeedAudio').addEventListener('click', function() {
    speed = document.getElementById('inputspeedaudio').value;
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      chrome.tabs.sendMessage(
          tabs[0].id,
          {from: 'popup', subject: 'Audio', speed: speed,},
          function(response){});
    });
  });

  document.getElementById('resetVideo').addEventListener('click', function() {
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      chrome.tabs.sendMessage(
          tabs[0].id,
          {from: 'popup', subject: 'Video', speed: 1.0,},
          function(response){});
    });
  });
  document.getElementById('resetAudio').addEventListener('click', function() {
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      chrome.tabs.sendMessage(
          tabs[0].id,
          {from: 'popup', subject: 'Audio', speed: 1.0,},
          function(response){});
    });
  });
});

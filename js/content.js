// Inform the background page that
// this tab should have a page-action
chrome.runtime.sendMessage({
  from:    'content',
  subject: 'showPageAction'
});

// Listen for messages from the popup
chrome.runtime.onMessage.addListener(function (msg, sender, response) {
  if (msg.from === 'dataload') {
    if (msg.pause) {
      iFrameBuster(msg);
      setValuesSafe(0,0);
    }
    response(getDataValues());
  }
  if ((msg.from === 'popup') && (msg.subject === 'Video')) {
    iFrameBuster(msg);
    document.querySelector('video').playbackRate = msg.speed;
    response('success');
  }
  if ((msg.from === 'popup') && (msg.subject === 'Audio')) {
    iFrameBuster(msg);
    document.querySelector('audio').playbackRate = msg.speed;
    response('success');
  }
});

function getDataValues() {
  try {
    videoSpeed = document.querySelector('video').playbackRate;
  } catch (err) {
    videoSpeed = 0.0;
  }
  try {
    audioSpeed = document.querySelector('audio').playbackRate;
  } catch (err) {
    audioSpeed = 0.0;
  }
  console.log(videoSpeed);
  return JSON.stringify({
    video: videoSpeed,
    audio: audioSpeed
  });
}

function setValuesSafe(audio, video) {
  try {
    document.querySelector('audio').playbackRate = audio;
  } catch (err) {}
  try {
    document.querySelector('video').playbackRate = video;
  } catch (err) {}
}

function iFrameBuster(msg) {
  // hack to bust through iFrames
  var frames = document.getElementsByTagName('iframe');
  var message = JSON.stringify({
    event: 'command',
    func: 'setPlaybackRate',
    args: [msg.speed, true]
  });
  for (var i in frames) {
    try {
      frames[i].contentWindow.postMessage(message, '*');
    } catch (err) {
        console.log('error in iframe: ', err);
    }
  }
}
